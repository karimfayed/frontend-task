# Pixel Art Maker Project

## Table of Contents

* [Instructions](#instructions)
* [Contributing](#contributing)

## Instructions

To get started, open `designs.js` and start building out the app's functionality.

Technologies used:
  - HTML
  - CSS
  - JavaScript
  
User creates a nxm grid table with each cell can be accessed separately and color modified
accroding to the color picker's color.   
  

