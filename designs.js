// Select color input
// Select size input

// When size is submitted by the user, call makeGrid()
    document.getElementById("myBtn").addEventListener("click", makeGrid);

/**
* @description Sets the color of the selected table's cell to the value assigned to colorPicker
*/
function setColor(e)
{
    e.preventDefault();
    var backColor = document.getElementById("colorPicker").value;  // getting the value assigned to colorPicker and saving it in backColor
    var cell = document.getElementById(e.target.id);               // getting cell's unique ID and saving it in cell
    cell.style.backgroundColor = backColor;                        // assigning the value of backColor to the cell's background
}
/**
* @description Creates a rows x cols table corresponding to user's input
*/
function makeGrid(e)
{
    e.preventDefault();
    var x, y, table, row, col;                           // variables declaration
    rows = document.getElementById('inputHeight').value; // user's height input
    cols = document.getElementById('inputWidth').value;  // user's width input
    table = document.getElementById('pixelCanvas');
    table.innerHTML="";                                 // clear table's body before creation in case of existence of old table

    for (var i = 0; i < rows; i++)                      // outer for loop for rows creation
    {
        row = document.createElement('tr');

            for (var j = 0; j < cols; j++)   // inner for loop for dividing each row into number of columns required
            {
                col = document.createElement('td');
                col.addEventListener("click", setColor); // adding an event listener for each cell created to be able to access each cell and change its color
                col.id = "" + i +"" + j;                 // assigning a unique ID for each cell to be able to access each cell seperatly
                row.appendChild(col);
            }

        table.appendChild(row);
    }
        

}

